﻿using NUnit.Framework;
using University.DataClasses;

namespace University.Common
{
    public class UniversityBaseFixture
    {
        private static readonly string MasterKey = TestContext.Parameters["MasterKey"];

        protected UniversitySettings Settings { get; } = new UniversitySettings(MasterKey);

    }
}
