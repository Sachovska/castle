﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using University.Utils;

namespace University.DataClasses
{
    public class UniversitySettings
    {
        private static UniversitySettings _current;
        private static readonly string MasterKey = TestContext.Parameters["MasterKey"];

        public UniversitySettings(string masterkey = null, string currentProject = null)
        {
            const string encrypted = "Encrypted";
            const string settingsName = "settings.json";
            string customSettingsName;
            string defaultSettingsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, settingsName);

            if (currentProject != null)
            {
                // In case EfmSettings are needed before test is initialized, we pass project name to get proper custom settings file name
                customSettingsName = $"{currentProject}.{settingsName}";
            }
            else
            {
                // We use TestExecutionContext here because AppDomain will provide different info under ReSharper and NUnit Console execution runtime
                customSettingsName =
                    $"{TestExecutionContext.CurrentContext.CurrentTest.TypeInfo.Assembly.GetName().Name}.{settingsName}";
            }
            string customSettingsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, customSettingsName);
            string result = File.Exists(customSettingsPath)
                ? Tools.Json.MergeJsonFiles(defaultSettingsPath, customSettingsPath)
                : File.ReadAllText(defaultSettingsPath);
            var jsonDoc = Tools.Json.Deserialize<JObject>(result);

            // If MasterKey has been specified in run configuration, settings.json properties, that have prefix "Encrypted"
            // (i.e. "EncryptedPassword") are decrypted with custom DecryptAes256() method
            if (!string.IsNullOrEmpty(masterkey))
            {
                var jProperties = jsonDoc.Properties().ToList();
                foreach (var property in jProperties)
                {
                    if (property.Name.StartsWith(encrypted))
                    {
                        var decryptedValue = Tools.Encryption.DecryptAes256(property.Value.ToString(), masterkey);
                        var oldName = property.Name;
                        var newName = oldName.Remove(0, encrypted.Length);
                        property.Replace(new JProperty(newName, decryptedValue));
                    }
                }
            }
            var updatedResult = jsonDoc.ToString();
            Tools.Json.PopulateJson(updatedResult, this);
        }

        public string Browser { get; set; }

        public string AuditoriesHost { get; set; }

        public string StudentsHost { get; set; }

        public static UniversitySettings GetCurrent(string currentProject = null)
        {
            _current = _current ?? new UniversitySettings(MasterKey, currentProject);
            return _current;
        }
    }
}
