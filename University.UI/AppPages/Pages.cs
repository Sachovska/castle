﻿using OpenQA.Selenium.Support.PageObjects;
using University.UI.Wrappers;

namespace University.UI.AppPages
{
    public static class Pages
    {
        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(BrowserFactory.Driver, page);
            return page;
        }

        public static PageCommon PageCommon => GetPage<PageCommon>();

        public static PageAuditories PageAuditories => GetPage<PageAuditories>();

        public static PageStudents PageStudents => GetPage<PageStudents>();
    }
}
