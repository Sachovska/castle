﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace University.UI.AppPages
{
    public class PageCommon
    {
        private IWebDriver _webDriver;

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light success']")]
        public IWebElement BtnAdd { get; set; }

        //[FindAll(How = How.XPath, Using = "//form//div[@class='v-input__icon v-input__icon--append']//i")]
        //public List<IWebElement> BtnExpand { get; set; }
    }
}
