﻿using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using University.UI.Wrappers;

namespace University.UI.AppPages
{
    public class PageStudents
    {

        #region Properties of Students page

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light success']")]
        public IWebElement BtnAdd { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light info']")]
        public IWebElement BtnSave { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light error']")]
        public IWebElement BtnDelete { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn v-btn--flat theme--light error--text text--darken-1']")]
        public IWebElement BtnYes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Name']")]
        public IWebElement InputName { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='RecordBook']")]
        public IWebElement InputRecordBook { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='StudentIdCard']")]
        public IWebElement InputStudentIdCard { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Email']")]
        public IWebElement InputEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Phone']")]
        public IWebElement InputPhone { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='User']")]
        public IWebElement InputUser { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Group']")]
        public IWebElement InputGroup { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Search']")]
        public IWebElement InputSearch { get; set; }

        public ReadOnlyCollection<IWebElement> TableItems =>
            BrowserFactory.Driver.FindElements(
                By.XPath("//table[@class='v-datatable v-table theme--light']//tbody/tr"));

        public ReadOnlyCollection<IWebElement> BtnsExpand =>
            BrowserFactory.Driver.FindElements(
                By.XPath("//form//div[@class='v-input__icon v-input__icon--append']//i"));

        public ReadOnlyCollection<IWebElement> LblWarningMessage =>
            BrowserFactory.Driver.FindElements(By.XPath("//div[@class='v-messages__message']"));

        [FindsBy(How = How.XPath,
            Using = "//div[@class='v-counter error--text theme--light']")]
        public IWebElement LblLengthWarning;

        #endregion

        /// <summary>
        /// Add new student
        /// </summary>
        /// <param name="name">surname, name, middle (last) name</param>
        /// <param name="recordBook">record book</param>
        /// <param name="studentIdCard">id card of student</param>
        /// <param name="email">student email</param>
        /// <param name="phone">student phone number</param>
        /// <param name="user">user in system</param>
        /// <param name="group">student group</param>
        public void AddStudent(string name, string recordBook, string studentIdCard, string email, string phone,
            string user = "102702", string group = "101")
        {
            BtnAdd.Click();
            InputName.SendKeys(name);
            InputRecordBook.Clear();
            InputRecordBook.SendKeys(recordBook);
            InputStudentIdCard.Clear();
            InputStudentIdCard.SendKeys(studentIdCard);
            InputEmail.SendKeys(email);
            InputPhone.SendKeys(phone);
            BtnsExpand.First().Click();
            SelectItemByText(user);
            BtnsExpand.Last().Click();
            SelectItemByText(group);
            BtnSave.Click();
        }

        /// <summary>
        /// Select item by text
        /// </summary>
        /// <param name="value"></param>
        public void SelectItemByText(string value)
        {
            var elements =
                BrowserFactory.Driver.FindElements(
                    By.XPath("//div[@class='v-list theme--light']//a//div[@class='v-list__tile__title']"));
            foreach (var e in elements)
            {
                if (e.Text.Equals(value))
                {
                    e.Click();
                }
            }
        }
    }
}
