﻿using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using University.UI.Wrappers;

namespace University.UI.AppPages
{
    public class PageAuditories
    {
        #region Properties of Auditory page

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light success']")]
        public IWebElement BtnAdd { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light info']")]
        public IWebElement BtnSave { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn theme--light error']")]
        public IWebElement BtnDelete { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='v-btn v-btn--flat theme--light error--text text--darken-1']")]
        public IWebElement BtnYes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Auditory name']")]
        public IWebElement InputAuditoryName { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Amount']")]
        public IWebElement InputAmount { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//*[@class='v-form']//input[@aria-label='Description']")]
        public IWebElement InputDescription { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Search']")]
        public IWebElement InputSearch { get; set; }

        public ReadOnlyCollection<IWebElement> TableItems =>
            BrowserFactory.Driver.FindElements(
                By.XPath("//table[@class='v-datatable v-table theme--light']//tbody/tr"));
        
        public ReadOnlyCollection<IWebElement> BtnsExpand =>
            BrowserFactory.Driver.FindElements(
                By.XPath("//form//div[@class='v-input__icon v-input__icon--append']//i"));

        public ReadOnlyCollection<IWebElement> LblWarningMessage =>
            BrowserFactory.Driver.FindElements(By.XPath("//div[@class='v-messages__message']"));

        [FindsBy(How = How.XPath,
            Using = "//div[@class='v-counter error--text theme--light']")]
        public IWebElement LblLengthWarning;
        #endregion

        /// <summary>
        /// Add new auditory
        /// </summary>
        /// <param name="name"></param>
        /// <param name="amountPlace"></param>
        /// <param name="description"></param>
        /// <param name="buildingNumber"></param>
        /// <param name="classType"></param>
        public void AddAuditory(string name, string amountPlace, string description, string buildingNumber = "1",
            string classType = "COMPUTER_CLASS")
        {
            BtnAdd.Click();
            InputAuditoryName.SendKeys(name);
            InputAmount.SendKeys(amountPlace);
            BtnsExpand.First().Click();
            SelectItemByText(buildingNumber);
            BtnsExpand.Last().Click();
            SelectItemByText(classType);
            InputDescription.SendKeys(description);
            BtnSave.Click();
        }

        /// <summary>
        /// Select item by text
        /// </summary>
        /// <param name="value"></param>
        public void SelectItemByText(string value)
        {
            var elements =
                BrowserFactory.Driver.FindElements(
                    By.XPath("//div[@class='v-list theme--light']//a//div[@class='v-list__tile__title']"));
            foreach (var e in elements)
            {
                if (e.Text.Equals(value))
                {
                    e.Click();
                }
            }
        }
    }
}
