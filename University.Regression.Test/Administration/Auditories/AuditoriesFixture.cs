﻿using System.Linq;
using System.Threading;
using NUnit.Framework;
using University.Common;
using University.UI.AppPages;
using University.UI.Wrappers;
using University.Utils;

namespace University.Regression.Test.Administration.Auditories
{
    [TestFixture]
    public class AuditoriesFixture : UniversityBaseFixture
    {
        [Test]
        public void CreateNewAuditory()
        {
            const string amountPlace = "12";
            string uniqueName = Tools.String.GetUniqueName($"Unique", 1);
            string auditoryName = $"{uniqueName}A";
            string description = $"{uniqueName}des";
            string expectedItemText = $"{auditoryName} {amountPlace} 1 COMPUTER_CLASS {description}";

            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.AuditoriesHost);

            // Create new auditory > Observe count of grid items
            Thread.Sleep(2000);
            int actualCountOfTable = Pages.PageAuditories.TableItems.Count;
            Pages.PageAuditories.AddAuditory(auditoryName, amountPlace, description);
            Thread.Sleep(2000);
            int expectedCountOfTable = Pages.PageAuditories.TableItems.Count;
            int countAfterAddedNewOne = actualCountOfTable + 1;
            Assert.AreEqual(expectedCountOfTable, countAfterAddedNewOne);
            
            // Search auditory by Name
            Pages.PageAuditories.InputSearch.SendKeys(auditoryName);
            var actualText = Pages.PageAuditories.TableItems.First().Text;
            Assert.AreEqual(expectedItemText, actualText);

            // Clean up
            Pages.PageAuditories.BtnDelete.Click();
            Pages.PageAuditories.BtnYes.Click();
            BrowserFactory.CloseAllDrivers();
        }

        [Test]
        public void VerificationWarningMessages()
        {
            const string warningOfNameEmptyField = "Name is required";
            const string warningOfNameLengthFiled = "Name must be less than 10 characters";
            const string warningOfAllowedLength = "33 / 10";
            const string warningOfAmountEmptyField = "Amount is required";
            string uniqueName = Tools.String.GetUniqueName();
            string auditoryName = $"{uniqueName}A";

            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.AuditoriesHost);

            // Set focus on 'Audotiry name' > Uselect > Observe warning message
            Thread.Sleep(2000);
            Pages.PageAuditories.BtnAdd.Click();
            Pages.PageAuditories.InputAuditoryName.SendKeys(string.Empty);
            Pages.PageAuditories.InputDescription.SendKeys(string.Empty);
            Thread.Sleep(2000);
            var actualWarning = Pages.PageAuditories.LblWarningMessage.First().Text;
            Assert.AreEqual(warningOfNameEmptyField, actualWarning);

            // Set name auditory more than 10 letters > Observe error message
            Pages.PageAuditories.InputAuditoryName.SendKeys(auditoryName);
            Pages.PageAuditories.InputDescription.SendKeys(string.Empty);
            Thread.Sleep(2000);
            actualWarning = Pages.PageAuditories.LblWarningMessage.First().Text;
            Assert.AreEqual(warningOfNameLengthFiled, actualWarning);
            actualWarning = Pages.PageAuditories.LblLengthWarning.Text;
            Assert.AreEqual(warningOfAllowedLength, actualWarning);

            // Set empty amount > Observe warning
            Pages.PageAuditories.InputAmount.SendKeys(string.Empty);
            Pages.PageAuditories.InputDescription.SendKeys(string.Empty);
            Thread.Sleep(2000);
            actualWarning = Pages.PageAuditories.LblWarningMessage.Last().Text;
            Assert.AreEqual(warningOfAmountEmptyField, actualWarning);
            
            // Clean up
            BrowserFactory.CloseAllDrivers();
        }
        
        [Test]
        public void AuditoryCreationWithoutData()
        {
            const string nameValidationMessage = "Name is required";
            const string amountValidationMessage = "Amount is required";
            const string buildingValidationMessage = "BuildingId is required";
            const string typeValidationMessage = "TypeId is required";
            const int countOfValidationMessages = 4;
            const int secondIndex = 1;
            const int thirdIndex = 2;

            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.AuditoriesHost);

            // Click 'Add' new auditory > Click 'Save' > Observe validations error message
            Thread.Sleep(2000);
            Pages.PageAuditories.BtnAdd.Click();
            Pages.PageAuditories.BtnSave.Click();
            Thread.Sleep(2000);
            var warningMessages = Pages.PageAuditories.LblWarningMessage.Select(message => message.Text).ToList();
            Assert.AreEqual(countOfValidationMessages, warningMessages.Count);
            Assert.AreEqual(nameValidationMessage, warningMessages.First());
            Assert.AreEqual(amountValidationMessage, warningMessages[secondIndex]);
            Assert.AreEqual(buildingValidationMessage, warningMessages[thirdIndex]);
            Assert.AreEqual(typeValidationMessage, warningMessages.Last());

            // Clean up
            BrowserFactory.CloseAllDrivers();
        }
    }
}
