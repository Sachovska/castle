﻿using System.Linq;
using System.Threading;
using NUnit.Framework;
using University.Common;
using University.UI.AppPages;
using University.UI.Wrappers;
using University.Utils;

namespace University.Regression.Test.Administration.Students
{
    [TestFixture]
    public class StudentsFixture : UniversityBaseFixture
    {

        [Test]
        public void StudentCreationWithoutData()
        {
            const string nameValidationMessage = "Name is required";
            const string recordBookValidationMessage = "RecordBook must be valid";
            const string studentIdCardValidationMessage = "StudentIdCard must be valid";
            const string emailValidationMessage = "E-mail is required";
            const string phoneValidationMessage = "Phone must be valid";
            const string groupValidationMessage = "Group is required";
            const int countOfValidationMessages = 6;
            const int secondIndex = 1;
            const int thirdIndex = 2;
            const int fourthIndex = 3;
            const int fifthIndex = 4;

            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.StudentsHost);

            // Click 'Add' new students > Click 'Save' > Observe validations error message
            Thread.Sleep(2000);
            Pages.PageStudents.BtnAdd.Click();
            Pages.PageStudents.BtnSave.Click();
            Thread.Sleep(2000);
            var warningMessages = Pages.PageStudents.LblWarningMessage.Select(message => message.Text).ToList();
            Assert.AreEqual(countOfValidationMessages, warningMessages.Count);
            Assert.AreEqual(nameValidationMessage, warningMessages.First());
            Assert.AreEqual(recordBookValidationMessage, warningMessages[secondIndex]);
            Assert.AreEqual(studentIdCardValidationMessage, warningMessages[thirdIndex]);
            Assert.AreEqual(emailValidationMessage, warningMessages[fourthIndex]);
            Assert.AreEqual(phoneValidationMessage, warningMessages[fifthIndex]);
            Assert.AreEqual(groupValidationMessage, warningMessages.Last());
            
            // Clean up
            BrowserFactory.CloseAllDrivers();
        }

        [Test]
        public void CreateNewStudent()
        {
            const string recordId = "123456";
            const string studentIdCard = "123456";
            const string phoneOfStudent = "123456";
            string uniqueName = Tools.String.GetUniqueName($"Unique", 1);
            string studentName = $"{uniqueName}stud";
            string emailOfStudent = Tools.String.GetUniqueEmail("email", 2);
            string expectedStudent = $"{studentName} {emailOfStudent} {phoneOfStudent}";

            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.StudentsHost);

            // Add new student
            Thread.Sleep(2000);
            int expectedCountOfStudents = Pages.PageStudents.TableItems.Count;
            Pages.PageStudents.AddStudent(studentName, recordId, studentIdCard, emailOfStudent, phoneOfStudent);
            Thread.Sleep(2000);
            int actualCountOfStudent = Pages.PageStudents.TableItems.Count;
            Assert.AreEqual(expectedCountOfStudents + 1, actualCountOfStudent);

            // Search student
            Pages.PageStudents.InputSearch.SendKeys(studentName);
            var actualStudent = Pages.PageStudents.TableItems.First().Text;
            Assert.AreEqual(expectedStudent, actualStudent);

            // Clean up
            Pages.PageStudents.BtnDelete.Click();
            Pages.PageStudents.BtnYes.Click();
            BrowserFactory.CloseAllDrivers();
        }
    }
}
