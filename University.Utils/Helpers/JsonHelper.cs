﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace University.Utils.Helpers
{
    public class JsonHelper
    {
        public T Deserialize<T>(string json)
        {
            T deserializedObject = JsonConvert.DeserializeObject<T>(json);
            return deserializedObject;
        }

        public string Serialize(object target)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            string json = JsonConvert.SerializeObject(target, settings);
            return json;
        }

        public string MergeJson(string baseFile, string updatedFile)
        {
            var mergeSettings = new JsonMergeSettings
            {
                MergeArrayHandling = MergeArrayHandling.Union
            };

            JObject updatedJson = JObject.Parse(updatedFile);
            JObject baseJson = JObject.Parse(baseFile);
            baseJson.Merge(updatedJson, mergeSettings);

            return baseJson.ToString();
        }

        public string MergeJsonFiles(string baseFile, string updatedFile)
        {
            var mergeSettings = new JsonMergeSettings
            {
                MergeArrayHandling = MergeArrayHandling.Union,
                MergeNullValueHandling = MergeNullValueHandling.Merge
            };

            JObject updatedJson = JObject.Parse(File.ReadAllText(updatedFile));
            JObject baseJson = JObject.Parse(File.ReadAllText(baseFile));
            baseJson.Merge(updatedJson, mergeSettings);

            return baseJson.ToString();
        }

        public void PopulateJson(string value, object target)
        {
            JsonConvert.PopulateObject(value, target);
        }

    }
}

