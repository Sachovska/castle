﻿using System;
using Castle.University.Utils;

namespace University.Utils.Helpers
{
    public class StringHelper
    {
        internal StringHelper()
        {
        }

        public string GetUniqueId(int requiredLength = 0)
        {
            string uniqueStr = Guid.NewGuid().ToString("N");

            return requiredLength > 0 && requiredLength < uniqueStr.Length ? uniqueStr.Substring(0, requiredLength) : uniqueStr;
        }

        public string GetUniqueEmail(string specificEmailPrefix, int requiredLength = 3)
        {
            return $"{specificEmailPrefix}{Tools.String.GetUniqueId(requiredLength)}@mail.com";
        }

        public string GetUniqueString(int requiredLength = 0)
        {
            long ticks = DateTime.Now.Ticks;
            byte[] bytes = BitConverter.GetBytes(ticks);
            string uniqueStr = Convert.ToBase64String(bytes)
                .Replace('+', '_')
                .Replace('/', '-')
                .TrimEnd('=');

            return requiredLength > 0 && requiredLength < uniqueStr.Length ? uniqueStr.Substring(0, requiredLength) : uniqueStr;
        }

        public string GetUniqueName([System.Runtime.CompilerServices.CallerMemberName] string memberName = "", int uniqueId = 4)
        {
            return $"{memberName}_{Tools.String.GetUniqueId(uniqueId)}";
        }
    }
}
