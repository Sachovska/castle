﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace University.Utils.Helpers
{
    public class EncryptionHelper
    {
        internal EncryptionHelper()
        {
        }

        public string DecryptAes256(string encryptedText, string masterKey)
        {
            byte[] cipherText = Convert.FromBase64String(encryptedText);
            byte[] IV = cipherText.Take(16).ToArray();
            byte[] key = Convert.FromBase64String(masterKey);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Create an AesManaged object with the specified key and IV
            AesManaged aesAlg = new AesManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.Zeros,
                BlockSize = 128,
                KeySize = 256,
                Key = key,
                IV = IV
            };

            // Create a decrytor to perform the stream transformation
            ICryptoTransform decryptor = aesAlg.CreateDecryptor();
            var decryptedValue = decryptor.TransformFinalBlock(cipherText, 16, cipherText.Length - 16);
            aesAlg.Dispose();
            var plaintext = Encoding.UTF8.GetString(decryptedValue).Trim("\0".ToCharArray());
            return plaintext;
        }
    }
}

