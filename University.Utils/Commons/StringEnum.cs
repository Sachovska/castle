﻿using System;
using System.Collections;
using System.Reflection;

namespace Castle.University.Utils.Commons
{
    public class StringEnum
    {
        #region Instance implementation

        private readonly Type _enumType;
        private static readonly Hashtable StringValues = new Hashtable();


        public StringEnum(Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException(string.Format("Supplied type must be an Enum.  Type was {0}", enumType));

            _enumType = enumType;
        }

        public string GetStringValue(string valueName)
        {
            string stringValue = null;
            try
            {
                Enum enumType = (Enum)Enum.Parse(_enumType, valueName);
                stringValue = GetStringValue(enumType);
            }
            catch (Exception)
            {
                // ignored
            }

            return stringValue;
        }

        #endregion

        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();

            if (StringValues.ContainsKey(value))
                output = (StringValues[value] as StringValueAttribute).Value;
            else
            {
                //Look for our 'StringValueAttribute' in the field's custom attributes
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attrs.Length > 0)
                {
                    StringValues.Add(value, attrs[0]);
                    output = attrs[0].Value;
                }
            }
            return output;
        }

        public static Enum GetEnumValue(Type enumType, string value)
        {
            value = value.Trim().ToLower();
            foreach (Enum enumItem in Enum.GetValues(enumType))
            {
                if (GetStringValue(enumItem).Trim().ToLower() == value)
                {
                    return enumItem;
                }
            }
            throw new Exception($"Value '{value}' is not defined!");
        }
    }

    public class StringValueAttribute : Attribute
    {
        private readonly string _value;

        public StringValueAttribute(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
