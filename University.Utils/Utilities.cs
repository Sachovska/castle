﻿using University.Utils.Helpers;

namespace University.Utils
{
    public static class Tools
    {
        private static readonly StringHelper _stringHelper = new StringHelper();
        private static readonly JsonHelper _jsonHelper = new JsonHelper();
        private static readonly EncryptionHelper _encryptionHelper = new EncryptionHelper();

        public static StringHelper String => _stringHelper;

        public static JsonHelper Json => _jsonHelper;

        public static EncryptionHelper Encryption => _encryptionHelper;
    }
}
